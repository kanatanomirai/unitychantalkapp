# UnitychanTalkApp

Unityちゃんとブラウザ上で会話が出来るアプリのソースコードです。  
このアプリはRuby on Rails 、Unity 、JavaScript を組み合わせて作られています。  
  
アプリURL:https://unitychan-talk.herokuapp.com/  
  
詳しくは[こちらの記事](https://qiita.com/kanatano_mirai/items/ecb826b06c658bf350a8)をご覧ください。
