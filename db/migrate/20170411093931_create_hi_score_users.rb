class CreateHiScoreUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :hi_score_users do |t|
      t.string :name
      t.integer :score

      t.timestamps
    end
  end
end
