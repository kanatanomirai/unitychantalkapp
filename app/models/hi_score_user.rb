class HiScoreUser < ApplicationRecord

  score_min = 0
  score_max = 999
  length_max = 15
  
  # 名前は15文字以内、スコアは0以上999以下
  validates :name, presence: true, length: { maximum: length_max }
  validates :score, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: score_min, less_than_or_equal_to: score_max }
end
