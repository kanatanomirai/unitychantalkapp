Rails.application.routes.draw do
  root 'front_pages#index'
  post '/', to: 'front_pages#create'
  get '/api', to: 'front_pages#connect'
end
